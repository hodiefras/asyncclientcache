﻿using System;

namespace Platform.Common.AsyncClientCache
{
    internal class CacheData<TOut>
    {
        public DateTime CreatedOnUtc { get; set; }

        public TOut Value { get; set; }

        public bool Valid { get; set; }
    }
}