﻿namespace Platform.Common.AsyncClientCache
{
    public enum CacheStrategy
    {
        AnyData = 0,
        ValidData = 1,
    }
}