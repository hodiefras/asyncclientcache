﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Platform.Common.AsyncClientCache
{
    /// <summary>
    /// https://stackoverflow.com/questions/31637394/is-concurrentdictionary-getoradd-guaranteed-to-invoke-valuefactorymethod-only/31637444#31637444
    /// </summary>
    public class AsyncLazy<T> : Lazy<Task<T>>
    {
        public AsyncLazy(Func<T> valueFactory)
            : base(() => Task.Factory.StartNew(valueFactory))
        {
        }

        public AsyncLazy(Func<Task<T>> taskFactory) 
            : base(() => Task.Factory.StartNew(taskFactory).Unwrap())
        {
        }

        public TaskAwaiter<T> GetAwaiter()
        {
            return Value.GetAwaiter();
        }
    }
}