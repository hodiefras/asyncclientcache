﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Platform.Common.AsyncClientCache
{
    /// <summary>
    ///     Async client cache based on ConcurrentDictionary and simulate CAS-operation
    /// </summary>
    public class DictionaryCache<TIn, TOut>
    {
        private readonly ConcurrentDictionary<TIn, AsyncLazy<CacheData<TOut>>> _cache =
            new ConcurrentDictionary<TIn, AsyncLazy<CacheData<TOut>>>();

        private readonly TimeSpan _cacheTimeout = TimeSpan.FromMinutes(30);
        private readonly CacheStrategy _strategy;

        public DictionaryCache(TimeSpan timeOut, CacheStrategy strategy = CacheStrategy.ValidData)
        {
            _cacheTimeout = timeOut;
            _strategy = strategy;
        }

        public DictionaryCache()
        {
        }

        /// <summary>
        /// Get cached value or add value with using factory method
        /// </summary>
        /// <param name="argument"></param>
        /// <param name="factoryFunc"></param>
        /// <returns></returns>
        public async Task<TOut> GetOrAddAsync(TIn argument, Func<TIn, Task<TOut>> factoryFunc)
        {
            if (argument == null)
            {
                throw new Exception($"{nameof(argument)} is null");
            }

            if (factoryFunc == null)
            {
                throw new Exception($"{nameof(factoryFunc)} is null");
            }

            var temp = await _cache.GetOrAdd(argument,
                k => CreateImmutableState(factoryFunc(argument)));
            var elapsed = DateTime.UtcNow - temp.CreatedOnUtc;
            if (elapsed > _cacheTimeout || (!temp.Valid && _strategy == CacheStrategy.ValidData))
            {
                _cache.TryRemove(argument,
                    out _);
                temp = await _cache.GetOrAdd(argument,
                    k => CreateImmutableState(factoryFunc(argument)));
            }

            return temp.Value;
        }

        /// <summary>
        /// Clear cache
        /// </summary>
        public void Clear()
        {
            foreach (var cachedPair in _cache)
            {
                _cache.TryRemove(cachedPair.Key, out var value);
            }
        }

        private AsyncLazy<CacheData<TOut>> CreateImmutableState(Task<TOut> factoryMethod)
        {
            return new AsyncLazy<CacheData<TOut>>(async () =>
            {
                TOut value = default(TOut);
                try
                {
                    value = await factoryMethod;
                }
                catch (Exception)
                {
                    return new CacheData<TOut>
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        Value = value,
                        Valid = false,
                    };
                }
                return new CacheData<TOut>
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    Value = value,
                    Valid = true,
                };
            });
        }
    }
}