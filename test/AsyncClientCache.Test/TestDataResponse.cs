﻿namespace Platform.Common.AsyncClientCache.Test
{
    public class TestDataResponse
    {
        public TestDataResponse()
        {
        }

        public TestDataResponse(TestDataRequest testDataRequest)
        {
            Name = testDataRequest.Name;
            Age = testDataRequest.Age;
            Marriage = testDataRequest.Marriage;
        }

        public string Name { get; set; }

        public int Age { get; set; }

        public bool Marriage { get; set; }

        public TestDataResponse Friend { get; set; }
    }
}