﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace Platform.Common.AsyncClientCache.Test
{
    public class DictionaryCacheTest
    {
        private const string ChangedFriendName = "Motherland";
        private const string ReturnValue = "TestString";
        private static Random random = new Random();

        private readonly Func<TestDataRequest, Task<TestDataResponse>> _soldierFunc = async s => await Task.Run(() =>
        {
            var response = new TestDataResponse(s)
            {
                Marriage = false,
                Age = 18,
                Friend = new TestDataResponse { Name = ChangedFriendName }
            };
            return response;
        });

        private readonly Func<TestDataRequest, Task<TestDataResponse>> _unmarriedFunc = async s => await Task.Run(() =>
        {
            var response = new TestDataResponse(s)
            {
                Marriage = false,
                Friend = new TestDataResponse { Marriage = false }
            };
            return response;
        });

        private readonly Func<TestDataRequest, Task<TestDataResponse>> _errorFunc = async s => await Task.Run(() =>
        {
            int i = random.Next(int.MaxValue);

            if (i % 2 == 0)
            {
                throw new Exception();
            }
            return new TestDataResponse();
        });

        [Test]
        public async Task ClientCache_GetAsyncWithError_ShouldBeOk()
        {
            // Arrange
            var key = TestDataGenerator.GenerateTestDataRequest();
            var results = new List<TestDataResponse>();
            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>();
            int n = 100;

            // Act
            for (int i = 0; i < n; i++)
            {
                var result = await asyncCache.GetOrAddAsync(key,
                    _errorFunc);
                results.Add(result);
            }

            // Assert
            results.Select((x, y) => (x != null && y != null) || (x == null && y != null) || (x != null && y == null)).Count().Should().Be(n);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueManyTimesWithSameParameter_ShouldCallOnlyOnce()
        {
            // Arrange
            var key = TestDataGenerator.GenerateTestDataRequest();

            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>();

            // Act
            var result = await asyncCache.GetOrAddAsync(key,
                _unmarriedFunc);
            var result2 = await asyncCache.GetOrAddAsync(key,
                _soldierFunc);
            
            // Assert
            result.Should().BeEquivalentTo(result2);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueManyTimesWithSameKeyButManyTimesCreatedValue_ShouldCallOnlyOnce()
        {
            // Arrange
            var key = 157;

            var key2 = 157;

            var asyncCache = new DictionaryCache<int, int>();
            int callCount = 0;

            // Act
            var result = await asyncCache.GetOrAddAsync(key,
                async (s) =>
                {
                    callCount++;
                    return await Task.FromResult<int>(151);
                });
            var result2 = await asyncCache.GetOrAddAsync(key2,
                async (s) =>
                {
                    callCount++;
                    return await Task.FromResult<int>(152);
                });

            // Assert
            result.Should().Be(result2);
            callCount.Should().Be(1);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueManyTimesWithSameKeyButManyTimes_ShouldCallOnlyOnce()
        {
            // Arrange
            var key = new TestDataRequest()
            {
                Age = 152,
            };

            var key2 = new TestDataRequest()
            {
                Age = 152,
            };

            var asyncCache = new DictionaryCache<int, int>();
            int callCount = 0;

            // Act
            var result = await asyncCache.GetOrAddAsync(key.GetHashCode(),
                async (s) =>
                {
                    callCount++;
                    return await Task.FromResult<int>(151);
                });
            var result2 = await asyncCache.GetOrAddAsync(key2.GetHashCode(),
                async (s) =>
                {
                    callCount++;
                    return await Task.FromResult<int>(152);
                });

            // Assert
            result.Should().Be(result2);
            callCount.Should().Be(1);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueManyTimesWithDifferentParameter_ShouldCallTwice()
        {
            // Arrange
            var key = TestDataGenerator.GenerateTestDataRequest();
            var key2 = TestDataGenerator.GenerateTestDataRequest();

            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>();

            // Act
            var result = await asyncCache.GetOrAddAsync(key,
                _unmarriedFunc);
            var result2 = await asyncCache.GetOrAddAsync(key2,
                _soldierFunc);

            // Assert
            result.Should().NotBeSameAs(result2);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueInManyTasks_ShouldBeOk()
        {
            // Arrange
            var asyncCache = new DictionaryCache<string, string>();

            // Act
            var resultTasks = Enumerable.Range(1,
                200).Select(async i => await asyncCache.GetOrAddAsync("key",
                async s => await Task.Run(() => ReturnValue))).ToArray();
            var results = await Task.WhenAll(resultTasks);

            // Assert
            results.Distinct().Should().ContainSingle(ReturnValue);
        }

        [Test]
        public async Task ClientCache_GetAsyncValueInManyTasks_ShouldBeCallOnceFirstTime()
        {
            // Arrange
            var asyncCache = new DictionaryCache<int, int>();
            var startNumber = 1;

            // Act
            var resultTasks = Enumerable.Range(startNumber,
                200).Select(async i => await asyncCache.GetOrAddAsync(startNumber,
                async s => await Task.Run(() => i))).ToArray();
            var results = await Task.WhenAll(resultTasks);

            // Assert
            results.Distinct().Should().BeEquivalentTo(startNumber);
        }

        [Test]
        public async Task AsyncCache_GetAsyncValueInManyTasks_ShouldBeOrdered()
        {
            // Arrange
            var asyncCache = new DictionaryCache<int, int>();
            var startNumber = 1;

            // Act
            var resultTasks = Enumerable.Range(startNumber,
                200).Select(async i => await asyncCache.GetOrAddAsync(i % 10,
                async s => await Task.Run(() => i))).ToArray();
            var results = await Task.WhenAll(resultTasks);

            // Assert
            results.Distinct().Should().BeInAscendingOrder();
        }

        [Test]
        public async Task AsyncCache_GetAsyncValueInManyTasks_ShouldBeInAscending()
        {
            // Arrange
            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>();
            var startNumber = 0;

            // Act
            var resultTasks = Enumerable.Range(startNumber,
                200).Select(async i => await asyncCache.GetOrAddAsync(new TestDataRequest
                {
                    Name = (i % 10).ToString()
                },
                async s => await Task.Run(() => new TestDataResponse(s)))).ToArray();
            var results = await Task.WhenAll(resultTasks);

            // Assert
            results.Select(t => Convert.ToInt32(t.Name)).Distinct().Should().BeInAscendingOrder();
        }

        [Test]
        public async Task AsyncCache_GetAsyncValueManyTimesWithDifferentParameterSmallTime_ShouldCallTwice()
        {
            // Arrange
            var key = TestDataGenerator.GenerateTestDataRequest();

            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>(new TimeSpan(0));

            // Act
            var result = await asyncCache.GetOrAddAsync(key,
                _unmarriedFunc);
            var result2 = await asyncCache.GetOrAddAsync(key,
                _soldierFunc);

            // Assert
            result.Should().NotBeSameAs(result2);
        }

        [Test]
        public async Task AsyncCache_ClearCache_ShouldBeNewValue()
        {
            // Arrange
            var key = TestDataGenerator.GenerateTestDataRequest();

            var asyncCache = new DictionaryCache<TestDataRequest, TestDataResponse>(TimeSpan.FromMinutes(10));

            // Act
            var result = await asyncCache.GetOrAddAsync(key,
                _unmarriedFunc);
            asyncCache.Clear();
            var result2 = await asyncCache.GetOrAddAsync(key,
                _soldierFunc);

            // Assert
            result.Should().NotBeSameAs(result2);
        }
    }
}