﻿using System;

namespace Platform.Common.AsyncClientCache.Test
{
    public class TestDataGenerator
    {
        private static readonly Random _random = new Random();

        public static TestDataRequest GenerateTestDataRequest()
        {
            return new TestDataRequest
            {
                Name = _random.Next(int.MaxValue).ToString(),
                Marriage = true,
                Age = _random.Next(int.MaxValue)
            };
        }
    }
}