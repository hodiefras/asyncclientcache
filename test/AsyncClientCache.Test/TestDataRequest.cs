﻿namespace Platform.Common.AsyncClientCache.Test
{
    public class TestDataRequest
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public bool Marriage { get; set; }

        public TestDataRequest Friend { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null,
                obj))
            {
                return false;
            }

            if (ReferenceEquals(this,
                obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((TestDataRequest)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Age;
                hashCode = (hashCode * 397) ^ Marriage.GetHashCode();
                hashCode = (hashCode * 397) ^ (Friend != null ? Friend.GetHashCode() : 0);
                return hashCode;
            }
        }

        protected bool Equals(TestDataRequest other)
        {
            return Name == other.Name && Age == other.Age && Marriage == other.Marriage && Equals(Friend,
                       other.Friend);
        }
    }
}